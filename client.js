/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
const util = require('./grpc');
var PROTO_PATH = __dirname + '/helloworld.proto';

var grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
var hello_proto = grpc.loadPackageDefinition(packageDefinition).helloworld;

function main() {
    // var client = new hello_proto.Greeter('localhost:50051',
    //     grpc.credentials.createInsecure());
    // var user;
    // user = 'world';
    // client.sayHello({name: user}, function (err, response) {
    //     console.log('Greeting:', response.message);
    // });
    // util.make_grpc_call('localhost:50051', hello_proto.Greeter.service.SayHello,
    //     {name: "BITCH"}, function (err, response) {
    //         if (err) {
    //             console.error(err);
    //         } else {
    //             console.log('Greeting:', response.message);
    //         }
    //     });

    let req = {name: "BITCH"};
    let req_serial = hello_proto.Greeter.service.SayHello.requestSerialize(req);
    util.make_grpc_call('localhost:50051',
        hello_proto.CloudProxy.service.proxy,
        {
            dest_url: "localhost:50052",
            request: req_serial
        }, function (err, response) {
            if (err) {
                console.error(err);
            } else {
                console.log('proxy-response:', response.error);
                console.log('proxy-response:', response.response);
                let resp = hello_proto.Greeter.service.SayHello.responseDeserialize(response.response);
                console.log(resp.message);
            }
        });
}

main();
